<?php
/**
 * @file
 * summit_session_eval.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function summit_session_eval_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create session_evals content'.
  $permissions['create session_evals content'] = array(
    'name' => 'create session_evals content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any session_evals content'.
  $permissions['delete any session_evals content'] = array(
    'name' => 'delete any session_evals content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own session_evals content'.
  $permissions['delete own session_evals content'] = array(
    'name' => 'delete own session_evals content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any session_evals content'.
  $permissions['edit any session_evals content'] = array(
    'name' => 'edit any session_evals content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own session_evals content'.
  $permissions['edit own session_evals content'] = array(
    'name' => 'edit own session_evals content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'use add another'.
  $permissions['use add another'] = array(
    'name' => 'use add another',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'addanother',
  );

  return $permissions;
}
