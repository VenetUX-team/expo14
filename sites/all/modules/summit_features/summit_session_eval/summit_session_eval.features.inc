<?php
/**
 * @file
 * summit_session_eval.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function summit_session_eval_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function summit_session_eval_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function summit_session_eval_node_info() {
  $items = array(
    'session_evals' => array(
      'name' => t('Session Evaluations'),
      'base' => 'node_content',
      'description' => t('This is where you enter session evaluations.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Please enter session evaluations using the form below.'),
    ),
  );
  return $items;
}
