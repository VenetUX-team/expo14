<?php
/**
 * @file
 * summit_session_eval.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function summit_session_eval_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'session_eval_data';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Session Evaluation Data';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Session Evaluation Data';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    2 => '2',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1000';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_seminar_name_target_id']['id'] = 'field_seminar_name_target_id';
  $handler->display->display_options['relationships']['field_seminar_name_target_id']['table'] = 'field_data_field_seminar_name';
  $handler->display->display_options['relationships']['field_seminar_name_target_id']['field'] = 'field_seminar_name_target_id';
  $handler->display->display_options['relationships']['field_seminar_name_target_id']['label'] = 'Session relationship';
  /* Field: Content: Session Number */
  $handler->display->display_options['fields']['field_session_number']['id'] = 'field_session_number';
  $handler->display->display_options['fields']['field_session_number']['table'] = 'field_data_field_session_number';
  $handler->display->display_options['fields']['field_session_number']['field'] = 'field_session_number';
  $handler->display->display_options['fields']['field_session_number']['relationship'] = 'field_seminar_name_target_id';
  $handler->display->display_options['fields']['field_session_number']['element_label_colon'] = FALSE;
  /* Field: Content: Seminar name */
  $handler->display->display_options['fields']['field_seminar_name']['id'] = 'field_seminar_name';
  $handler->display->display_options['fields']['field_seminar_name']['table'] = 'field_data_field_seminar_name';
  $handler->display->display_options['fields']['field_seminar_name']['field'] = 'field_seminar_name';
  $handler->display->display_options['fields']['field_seminar_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_seminar_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_seminar_name']['settings'] = array(
    'link' => 0,
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'field_speaker_target_id';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Clarity, Organization */
  $handler->display->display_options['fields']['field_eval_clarity_organization']['id'] = 'field_eval_clarity_organization';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['table'] = 'field_data_field_eval_clarity_organization';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['field'] = 'field_eval_clarity_organization';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['element_label_colon'] = FALSE;
  /* Field: Content: Presentation Quality */
  $handler->display->display_options['fields']['field_eval_oral_presentation']['id'] = 'field_eval_oral_presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['table'] = 'field_data_field_eval_oral_presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['field'] = 'field_eval_oral_presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['label'] = 'Oral Presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['element_label_colon'] = FALSE;
  /* Field: Content: Speaker Knowledge */
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['id'] = 'field_eval_speaker_knowledge';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['table'] = 'field_data_field_eval_speaker_knowledge';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['field'] = 'field_eval_speaker_knowledge';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['element_label_colon'] = FALSE;
  /* Field: Content: Matched Description */
  $handler->display->display_options['fields']['field_eval_matched_description']['id'] = 'field_eval_matched_description';
  $handler->display->display_options['fields']['field_eval_matched_description']['table'] = 'field_data_field_eval_matched_description';
  $handler->display->display_options['fields']['field_eval_matched_description']['field'] = 'field_eval_matched_description';
  $handler->display->display_options['fields']['field_eval_matched_description']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_matched_description']['element_label_colon'] = FALSE;
  /* Field: Content: Overall */
  $handler->display->display_options['fields']['field_eval_overall']['id'] = 'field_eval_overall';
  $handler->display->display_options['fields']['field_eval_overall']['table'] = 'field_data_field_eval_overall';
  $handler->display->display_options['fields']['field_eval_overall']['field'] = 'field_eval_overall';
  $handler->display->display_options['fields']['field_eval_overall']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_overall']['element_label_colon'] = FALSE;
  /* Field: Content: Comments */
  $handler->display->display_options['fields']['field_eval_comments']['id'] = 'field_eval_comments';
  $handler->display->display_options['fields']['field_eval_comments']['table'] = 'field_data_field_eval_comments';
  $handler->display->display_options['fields']['field_eval_comments']['field'] = 'field_eval_comments';
  $handler->display->display_options['fields']['field_eval_comments']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Seminar name (field_seminar_name) */
  $handler->display->display_options['sorts']['field_seminar_name_target_id']['id'] = 'field_seminar_name_target_id';
  $handler->display->display_options['sorts']['field_seminar_name_target_id']['table'] = 'field_data_field_seminar_name';
  $handler->display->display_options['sorts']['field_seminar_name_target_id']['field'] = 'field_seminar_name_target_id';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'session_evals' => 'session_evals',
  );
  /* Filter criterion: Content: Post date */
  $handler->display->display_options['filters']['created']['id'] = 'created';
  $handler->display->display_options['filters']['created']['table'] = 'node';
  $handler->display->display_options['filters']['created']['field'] = 'created';
  $handler->display->display_options['filters']['created']['operator'] = '>';
  $handler->display->display_options['filters']['created']['value']['value'] = '2013-10-01 00:00:00';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'session-evaluation-data';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9999';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'session-eval-data-export.csv';

  /* Display: Data export 2 */
  $handler = $view->new_display('views_data_export', 'Data export 2', 'views_data_export_2');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9999';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Session Number */
  $handler->display->display_options['fields']['field_session_number']['id'] = 'field_session_number';
  $handler->display->display_options['fields']['field_session_number']['table'] = 'field_data_field_session_number';
  $handler->display->display_options['fields']['field_session_number']['field'] = 'field_session_number';
  $handler->display->display_options['fields']['field_session_number']['relationship'] = 'field_seminar_name_target_id';
  $handler->display->display_options['fields']['field_session_number']['element_label_colon'] = FALSE;
  /* Field: Content: Seminar name */
  $handler->display->display_options['fields']['field_seminar_name']['id'] = 'field_seminar_name';
  $handler->display->display_options['fields']['field_seminar_name']['table'] = 'field_data_field_seminar_name';
  $handler->display->display_options['fields']['field_seminar_name']['field'] = 'field_seminar_name';
  $handler->display->display_options['fields']['field_seminar_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_seminar_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_seminar_name']['settings'] = array(
    'link' => 0,
  );
  /* Field: Content: Clarity, Organization */
  $handler->display->display_options['fields']['field_eval_clarity_organization']['id'] = 'field_eval_clarity_organization';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['table'] = 'field_data_field_eval_clarity_organization';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['field'] = 'field_eval_clarity_organization';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_clarity_organization']['element_label_colon'] = FALSE;
  /* Field: Content: Presentation Quality */
  $handler->display->display_options['fields']['field_eval_oral_presentation']['id'] = 'field_eval_oral_presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['table'] = 'field_data_field_eval_oral_presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['field'] = 'field_eval_oral_presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['label'] = 'Oral Presentation';
  $handler->display->display_options['fields']['field_eval_oral_presentation']['element_label_colon'] = FALSE;
  /* Field: Content: Speaker Knowledge */
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['id'] = 'field_eval_speaker_knowledge';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['table'] = 'field_data_field_eval_speaker_knowledge';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['field'] = 'field_eval_speaker_knowledge';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_speaker_knowledge']['element_label_colon'] = FALSE;
  /* Field: Content: Matched Description */
  $handler->display->display_options['fields']['field_eval_matched_description']['id'] = 'field_eval_matched_description';
  $handler->display->display_options['fields']['field_eval_matched_description']['table'] = 'field_data_field_eval_matched_description';
  $handler->display->display_options['fields']['field_eval_matched_description']['field'] = 'field_eval_matched_description';
  $handler->display->display_options['fields']['field_eval_matched_description']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_matched_description']['element_label_colon'] = FALSE;
  /* Field: Content: Overall */
  $handler->display->display_options['fields']['field_eval_overall']['id'] = 'field_eval_overall';
  $handler->display->display_options['fields']['field_eval_overall']['table'] = 'field_data_field_eval_overall';
  $handler->display->display_options['fields']['field_eval_overall']['field'] = 'field_eval_overall';
  $handler->display->display_options['fields']['field_eval_overall']['group_type'] = 'avg';
  $handler->display->display_options['fields']['field_eval_overall']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'session-eval-data-export-avg.csv';
  $export['session_eval_data'] = $view;

  $view = new view();
  $view->name = 'session_list_for_session_eval';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Session List for Session Eval';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Session Number */
  $handler->display->display_options['fields']['field_session_number']['id'] = 'field_session_number';
  $handler->display->display_options['fields']['field_session_number']['table'] = 'field_data_field_session_number';
  $handler->display->display_options['fields']['field_session_number']['field'] = 'field_session_number';
  $handler->display->display_options['fields']['field_session_number']['label'] = '';
  $handler->display->display_options['fields']['field_session_number']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Sort criterion: Content: Session Number (field_session_number) */
  $handler->display->display_options['sorts']['field_session_number_value']['id'] = 'field_session_number_value';
  $handler->display->display_options['sorts']['field_session_number_value']['table'] = 'field_data_field_session_number';
  $handler->display->display_options['sorts']['field_session_number_value']['field'] = 'field_session_number_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'session' => 'session',
  );

  /* Display: Entity Reference for Session Eval form */
  $handler = $view->new_display('entityreference', 'Entity Reference for Session Eval form', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['session_list_for_session_eval'] = $view;

  return $export;
}
