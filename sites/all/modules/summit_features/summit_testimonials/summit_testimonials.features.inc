<?php
/**
 * @file
 * summit_testimonials.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function summit_testimonials_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function summit_testimonials_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function summit_testimonials_node_info() {
  $items = array(
    'testimonial' => array(
      'name' => t('Testimonial'),
      'base' => 'node_content',
      'description' => t('Nice things people say about us.'),
      'has_title' => '1',
      'title_label' => t('Person\'s name and title'),
      'help' => '',
    ),
  );
  return $items;
}
