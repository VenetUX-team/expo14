<?php
/**
 * @file
 * summit_core.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function summit_core_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|speaker|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'speaker';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_speaker_title',
        1 => 'field_speaker_company',
        2 => 'field_speaker_headshot',
        3 => 'body',
        4 => 'field_speaker_faculty_level',
      ),
    ),
    'fields' => array(
      'field_speaker_title' => 'ds_content',
      'field_speaker_company' => 'ds_content',
      'field_speaker_headshot' => 'ds_content',
      'body' => 'ds_content',
      'field_speaker_faculty_level' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|speaker|default'] = $ds_layout;

  return $export;
}
