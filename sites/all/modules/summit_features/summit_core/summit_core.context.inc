<?php
/**
 * @file
 * summit_core.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function summit_core_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sessions';
  $context->description = 'Speakers related to sessions';
  $context->tag = 'Sessions';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'session/*' => 'session/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-speakers_on_session-block' => array(
          'module' => 'views',
          'delta' => 'speakers_on_session-block',
          'region' => 'content',
          'weight' => '-9',
        ),
        'sharethis-sharethis_block' => array(
          'module' => 'sharethis',
          'delta' => 'sharethis_block',
          'region' => 'content',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sessions');
  t('Speakers related to sessions');
  $export['sessions'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'speakers';
  $context->description = 'Sessions related to speakers';
  $context->tag = 'Speakers';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'speaker/*' => 'speaker/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-sessions_on_speaker-block' => array(
          'module' => 'views',
          'delta' => 'sessions_on_speaker-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sessions related to speakers');
  t('Speakers');
  $export['speakers'] = $context;

  return $export;
}
