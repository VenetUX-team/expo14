<?php
/**
 * @file
 * summit_flexslider.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function summit_flexslider_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function summit_flexslider_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function summit_flexslider_image_default_styles() {
  $styles = array();

  // Exported image style: front_page_slideshow.
  $styles['front_page_slideshow'] = array(
    'name' => 'front_page_slideshow',
    'label' => 'Front page slideshow',
    'effects' => array(
      5 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 780,
          'height' => 360,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function summit_flexslider_node_info() {
  $items = array(
    'frontpage_slideshow' => array(
      'name' => t('Frontpage Slideshow'),
      'base' => 'node_content',
      'description' => t('Images for the frontpage slideshow go here.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
