<?php
/**
 * @file
 * summit_flexslider.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function summit_flexslider_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide_desktop';
  $context->description = 'this sitewide context controls the desktop display';
  $context->tag = '';
  $context->conditions = array(
    'cmd' => array(
      'values' => array(
        3 => 3,
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-frontpage_slideshow-block' => array(
          'module' => 'views',
          'delta' => 'frontpage_slideshow-block',
          'region' => 'highlight',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('this sitewide context controls the desktop display');
  $export['sitewide_desktop'] = $context;

  return $export;
}
