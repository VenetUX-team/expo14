<?php
/**
 * @file
 * summit_sponsors.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function summit_sponsors_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function summit_sponsors_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function summit_sponsors_image_default_styles() {
  $styles = array();

  // Exported image style: sponsor_logo_footer.
  $styles['sponsor_logo_footer'] = array(
    'name' => 'sponsor_logo_footer',
    'label' => 'Sponsor logo footer',
    'effects' => array(
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 48,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function summit_sponsors_node_info() {
  $items = array(
    'sponsor' => array(
      'name' => t('Sponsor'),
      'base' => 'node_content',
      'description' => t('Logos and links for Summit event sponsors'),
      'has_title' => '1',
      'title_label' => t('Sponsor Company Name'),
      'help' => '',
    ),
  );
  return $items;
}
