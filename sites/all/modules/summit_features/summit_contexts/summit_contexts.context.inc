<?php
/**
 * @file
 * summit_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function summit_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front';
  $context->description = 'Only for Front Page';
  $context->tag = 'Front Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-4' => array(
          'module' => 'block',
          'delta' => '4',
          'region' => 'preface_first',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => '5',
          'region' => 'preface_second',
          'weight' => '-10',
        ),
        'block-6' => array(
          'module' => 'block',
          'delta' => '6',
          'region' => 'preface_third',
          'weight' => '-10',
        ),
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'header_wide',
          'weight' => '-10',
        ),
        'block-7' => array(
          'module' => 'block',
          'delta' => '7',
          'region' => 'preface_fourth',
          'weight' => '-10',
        ),
        'block-8' => array(
          'module' => 'block',
          'delta' => '8',
          'region' => 'preface_fifth',
          'weight' => '-10',
        ),
        'block-9' => array(
          'module' => 'block',
          'delta' => '9',
          'region' => 'preface_sixth',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Front Page');
  t('Only for Front Page');
  $export['front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'no_front';
  $context->description = 'All pages except front';
  $context->tag = 'Front Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~session/*' => '~session/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sharethis-sharethis_block' => array(
          'module' => 'sharethis',
          'delta' => 'sharethis_block',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('All pages except front');
  t('Front Page');
  $export['no_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = 'Headers and footers';
  $context->tag = 'Sitewide';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-social-media' => array(
          'module' => 'menu',
          'delta' => 'menu-social-media',
          'region' => 'user_first',
          'weight' => '-10',
        ),
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'user_second',
          'weight' => '-10',
        ),
        'block-15' => array(
          'module' => 'block',
          'delta' => '15',
          'region' => 'branding',
          'weight' => '-10',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'block-2' => array(
          'module' => 'block',
          'delta' => '2',
          'region' => 'header_first',
          'weight' => '-10',
        ),
        'jquery_countdown_timer-jquery_countdown_timer' => array(
          'module' => 'jquery_countdown_timer',
          'delta' => 'jquery_countdown_timer',
          'region' => 'header_second',
          'weight' => '-10',
        ),
        'twitter_block-1' => array(
          'module' => 'twitter_block',
          'delta' => '1',
          'region' => 'postscript_first',
          'weight' => '-10',
        ),
        'views-video_promos-block' => array(
          'module' => 'views',
          'delta' => 'video_promos-block',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
        'views-sponsor_logos-block' => array(
          'module' => 'views',
          'delta' => 'sponsor_logos-block',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'block-10' => array(
          'module' => 'block',
          'delta' => '10',
          'region' => 'brought_to_you_by',
          'weight' => '-10',
        ),
        'menu-menu-summit' => array(
          'module' => 'menu',
          'delta' => 'menu-summit',
          'region' => 'footer_sitemap1',
          'weight' => '-10',
        ),
        'menu-menu-menu-about' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-about',
          'region' => 'footer_sitemap2',
          'weight' => '-10',
        ),
        'menu-menu-menu-resources' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-resources',
          'region' => 'footer_sitemap3',
          'weight' => '-10',
        ),
        'menu-menu-menu-contact' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-contact',
          'region' => 'footer_sitemap4',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Headers and footers');
  t('Sitewide');
  $export['sitewide'] = $context;

  return $export;
}
