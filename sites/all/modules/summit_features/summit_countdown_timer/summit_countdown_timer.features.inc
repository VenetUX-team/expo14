<?php
/**
 * @file
 * summit_countdown_timer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function summit_countdown_timer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
