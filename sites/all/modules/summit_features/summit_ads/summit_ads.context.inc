<?php
/**
 * @file
 * summit_ads.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function summit_ads_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'advertisements';
  $context->description = 'this context controls the ad block placement in the sidebars. paths are exclusionary';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~node' => '~node',
        '~agenda' => '~agenda',
        '~speaker-grid' => '~speaker-grid',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_rotator_pos1-block' => array(
          'module' => 'views',
          'delta' => 'ad_rotator_pos1-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-ad_rotator_pos2-block' => array(
          'module' => 'views',
          'delta' => 'ad_rotator_pos2-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('theme');
  t('this context controls the ad block placement in the sidebars. paths are exclusionary');
  $export['advertisements'] = $context;

  return $export;
}
