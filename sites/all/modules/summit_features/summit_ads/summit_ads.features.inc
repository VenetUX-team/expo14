<?php
/**
 * @file
 * summit_ads.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function summit_ads_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function summit_ads_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function summit_ads_node_info() {
  $items = array(
    'ad' => array(
      'name' => t('Ad'),
      'base' => 'node_content',
      'description' => t('This is where button/banner ads go.'),
      'has_title' => '1',
      'title_label' => t('Ad title'),
      'help' => '',
    ),
  );
  return $items;
}
