<?php
/**
 * @file
 * summit_ads.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function summit_ads_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ad_rotator_pos1';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Ad Rotator Pos1';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Ad Link */
  $handler->display->display_options['fields']['field_ad_link']['id'] = 'field_ad_link';
  $handler->display->display_options['fields']['field_ad_link']['table'] = 'field_data_field_ad_link';
  $handler->display->display_options['fields']['field_ad_link']['field'] = 'field_ad_link';
  $handler->display->display_options['fields']['field_ad_link']['label'] = '';
  $handler->display->display_options['fields']['field_ad_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_ad_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ad_link']['click_sort_column'] = 'url';
  /* Field: Content: Ad Image */
  $handler->display->display_options['fields']['field_ad_image']['id'] = 'field_ad_image';
  $handler->display->display_options['fields']['field_ad_image']['table'] = 'field_data_field_ad_image';
  $handler->display->display_options['fields']['field_ad_image']['field'] = 'field_ad_image';
  $handler->display->display_options['fields']['field_ad_image']['label'] = '';
  $handler->display->display_options['fields']['field_ad_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_ad_image']['alter']['path'] = '[field_ad_link]';
  $handler->display->display_options['fields']['field_ad_image']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_ad_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ad_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ad_image']['settings'] = array(
    'image_style' => '180_wide',
    'image_link' => '',
  );
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ad' => 'ad',
  );
  /* Filter criterion: Content: Ad Position (field_ad_position) */
  $handler->display->display_options['filters']['field_ad_position_value']['id'] = 'field_ad_position_value';
  $handler->display->display_options['filters']['field_ad_position_value']['table'] = 'field_data_field_ad_position';
  $handler->display->display_options['filters']['field_ad_position_value']['field'] = 'field_ad_position_value';
  $handler->display->display_options['filters']['field_ad_position_value']['value'] = array(
    1 => '1',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['ad_rotator_pos1'] = $view;

  $view = new view();
  $view->name = 'ad_rotator_pos2';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Ad Rotator Pos2';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Ad Link */
  $handler->display->display_options['fields']['field_ad_link']['id'] = 'field_ad_link';
  $handler->display->display_options['fields']['field_ad_link']['table'] = 'field_data_field_ad_link';
  $handler->display->display_options['fields']['field_ad_link']['field'] = 'field_ad_link';
  $handler->display->display_options['fields']['field_ad_link']['label'] = '';
  $handler->display->display_options['fields']['field_ad_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_ad_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ad_link']['click_sort_column'] = 'url';
  /* Field: Content: Ad Image */
  $handler->display->display_options['fields']['field_ad_image']['id'] = 'field_ad_image';
  $handler->display->display_options['fields']['field_ad_image']['table'] = 'field_data_field_ad_image';
  $handler->display->display_options['fields']['field_ad_image']['field'] = 'field_ad_image';
  $handler->display->display_options['fields']['field_ad_image']['label'] = '';
  $handler->display->display_options['fields']['field_ad_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_ad_image']['alter']['path'] = '[field_ad_link]';
  $handler->display->display_options['fields']['field_ad_image']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_ad_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ad_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ad_image']['settings'] = array(
    'image_style' => '180_wide',
    'image_link' => '',
  );
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ad' => 'ad',
  );
  /* Filter criterion: Content: Ad Position (field_ad_position) */
  $handler->display->display_options['filters']['field_ad_position_value']['id'] = 'field_ad_position_value';
  $handler->display->display_options['filters']['field_ad_position_value']['table'] = 'field_data_field_ad_position';
  $handler->display->display_options['filters']['field_ad_position_value']['field'] = 'field_ad_position_value';
  $handler->display->display_options['filters']['field_ad_position_value']['value'] = array(
    2 => '2',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['ad_rotator_pos2'] = $view;

  return $export;
}
