<?php
/**
 * @file
 * summit_promotions.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function summit_promotions_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'video_promos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Video promos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Summit 2013 Highlights';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Promo Video URL */
  $handler->display->display_options['fields']['field_promo_video_embed']['id'] = 'field_promo_video_embed';
  $handler->display->display_options['fields']['field_promo_video_embed']['table'] = 'field_data_field_promo_video_embed';
  $handler->display->display_options['fields']['field_promo_video_embed']['field'] = 'field_promo_video_embed';
  $handler->display->display_options['fields']['field_promo_video_embed']['label'] = '';
  $handler->display->display_options['fields']['field_promo_video_embed']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_promo_video_embed']['click_sort_column'] = 'video_url';
  $handler->display->display_options['fields']['field_promo_video_embed']['type'] = 'video_embed_field_thumbnail';
  $handler->display->display_options['fields']['field_promo_video_embed']['settings'] = array(
    'image_style' => 'video_promo_screen',
    'image_link' => 'node',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'promo_video' => 'promo_video',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['video_promos'] = $view;

  return $export;
}
