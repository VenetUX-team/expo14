<?php
/**
 * @file
 * summit_promotions.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function summit_promotions_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-promo_video-body'
  $field_instances['node-promo_video-body'] = array(
    'bundle' => 'promo_video',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-promo_video-field_promo_video_embed'
  $field_instances['node-promo_video-field_promo_video_embed'] = array(
    'bundle' => 'promo_video',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Paste URL to video here.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'video_embed_field',
        'settings' => array(
          'description' => 1,
          'video_style' => 'normal',
        ),
        'type' => 'video_embed_field',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_promo_video_embed',
    'label' => 'Promo Video URL',
    'required' => 1,
    'settings' => array(
      'description_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'video_embed_field',
      'settings' => array(),
      'type' => 'video_embed_field_video',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Paste URL to video here.');
  t('Promo Video URL');

  return $field_instances;
}
