<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/* get rid of "No front page content has been created yet." message */
function summit_preprocess_page(&$vars, $hook) {
  if (isset($vars['is_front'])) {
    /* unset($vars['page']['content']['system_main']['default_message']); */
  }
}
/** !@DISCLAIMER:
 * The above could also be achieved by creating an empty view page display and setting the
 * front page node [config -> site info] to it's url. Unsetting the content in the theme layer is
 * considered a kludge.
 */

/**
 * Implements theme_menu_tree__menu_id()
 *
 * @func:
 *   Return HTML for Social Media menu
 *
 *    You may see a lot of these overrides. This core function is the only one that outputs inline html,
 *    rather than using a template file or a better structured theme function - we can't simply add a class
 *    to an available $classes array and expect it to be output on the ul.
 */
function summit_menu_tree__menu_social_media($vars) {
  return '<ul class="menu menu--social-media links inline">' . $vars['tree'] . '</ul>';
}

/**
 * @funcSet:
 *   Return HTML for Primary Menu and Link wrappers [ ul & li ].
 
 * @func:
 * - Main UL
 */
function summit_menu_tree__main_menu($vars) {
  return '<a href="#main-navigation" onclick="jQuery(this).toggleClass(\'active\');jQuery(jQuery(this).attr(\'href\')).slideToggle()" class="nav-toggle">Menu</a><nav id="main-navigation" class="main-navigation"><ul class="menu links inline menu--main-menu main-menu__top">' . $vars['tree'] . '</ul></nav>';
}

/** @func:
 * - Main LI
 */
function summit_menu_link__main_menu(array $vars) {
  $element = $vars['element'];
  $sub_menu = '';
  if ($element['#below']) {
    foreach ($element['#below'] as $key => $val) {
      if (is_numeric($key)) {
        $element['#below'][$key]['#theme'] = 'menu_link__main_menu_inner'; // Second level LI
      }
    }
    $element['#below']['#theme_wrappers'][0] = 'menu_tree__main_menu_inner'; // Second level UL
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . '</li>';
}

/** @func:
 * - Inner UL
 */
function summit_menu_tree__main_menu_inner($vars) {
    return '<ul class="menu main-menu__inner">' . $vars['tree'] . '</ul>';
}

/** @func:
 * - Inner LI
 */
function summit_menu_link__main_menu_inner($vars) {
  $element = $vars['element'];
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . '</li>';
}

/**
 * Implements theme_form_form_id_alter()
 *
 * @func:
 *  - Search Block Form
 */
function summit_form_search_block_form_alter(&$form, &$form_state, $form_id) {
  $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
  $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
  $form['search_block_form']['#size'] = 20;  // define size of the textfield
  $form['search_block_form']['#default_value'] = t('Search the site'); // Set a default value for the textfield
// @todo: update search submit button to image
  //$form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button
  //$form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');

  // Add extra attributes to the text box
  $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search the site';}";
  $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search the site') {this.value = '';}";
  // Prevent user from searching the default text
  $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search our site'){ alert('Please enter a search'); return false; }";
}

/**
 * Implements theme_preprocess_views_view__FIELD_DISPLAY_ID()
 *
 *
function summit_preprocess_views_view_field__schedule__page__title(&$vars) {
  dsm($vars);
}*/

/** !@DISCLAIMER:
 * DO NOT EVER [EVER!!!] CLOSE THE PHP IN THIS FILE!!! BAD THINGS CAN AND WILL HAPPEN TO YOUR DRUPAL SITE!!!
 *
 * \?>
 *
 */