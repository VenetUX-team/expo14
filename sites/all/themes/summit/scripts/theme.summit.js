(function ($) {

  Drupal.behaviors.mobileNavHandler = {
    attach: function (context, settings) {
      $('.menu--main-menu .expanded ul', context).siblings('a, .nolink').once('mobile-menu', function () {
        // Scope our selectors into variables
        var $this = $(this),
            dropdownLinks = $('.menu--main-menu .expanded > a, .menu--main-menu .expanded > .nolink');
            
        // Javascript animation is processor intensive, so we'll just switch classes and control animation with css
        $this.click(function () {
          dropdownLinks.not(this).removeClass('active-link');
          setTimeout(function() {
            $this.toggleClass('active-link');
          }, 600);
        })
      });
    }
  };
  
  Drupal.behaviors.mobileNavToggle = {
    attach: function (context, settings) {
      $('.nav-toggle').scrollAnchor();
    }
  };
  
  // Helper functions
  
  $.fn.scrollAnchor = function () {
    $(this).click(function( event ){
      //check if it has a hash (i.e. if it's an anchor link)
      event.preventDefault();
      if(this.hash){
        var hash = this.hash.substr(1),
            toElement = $("[id="+hash+"]"),
            toPosition = toElement.offset().top - $(this).outerHeight();
        //scroll to element
        $("body,html").animate({
          scrollTop : toPosition
        },600);
      }
    });
  }

})(jQuery);